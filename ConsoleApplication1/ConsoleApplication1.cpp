#include "stdafx.h"
#include <iostream>
using namespace std;

struct time
{
	unsigned int hour;
	unsigned int minute;
	unsigned int second;
};
bool error(struct time time1, struct time time2) 
{
	if ((time1.hour < 24) && (time2.hour > time1.hour) && (time2.hour < 24) && (time1.hour>=0)&& (time2.hour >= 0))
	{
		if ((time1.minute < 60) && (time2.minute < 60)&& (time1.minute >= 0) && (time2.minute >= 0))
		{
			if ((time1.second < 60) && (time2.second < 60) && (time1.second >= 0) && (time2.second >= 0))
			{
				return true;
			}
		}
	}
	else
		return false;
};
struct time timeSub (struct time time1, struct time time2, struct time result)
{
	if (time2.second < time1.second)
	{
		result.second = 60 - (time1.second - time2.second);
		if (time2.minute != 0)
		{
			--time2.minute;
		}
		else
		{
			time2.minute = 60 - 1;
			if (time2.hour != 0)
				--time2.hour;
			else
			{
				time2.hour = 24 - 1;
			}
		}
	}
	else if (time2.second == time1.second)
	{
		result.second = 0;
	}
	else
	{
		result.second = time2.second- time1.second;
	}


	if (time2.minute < time1.minute)
	{
		result.minute = 60 - (time1.minute - time2.minute);
		if (time2.hour != 0)
			--time2.hour;
		else
		{
			time2.hour = 24 - 1;
		}
	}
	else if (time2.minute == time1.minute)
	{
		result.minute = 0;
	}
	else
	{
		result.minute = time2.minute- time1.minute;
	}

	result.hour = time2.hour- time1.hour;
	return result;
};

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	struct time timeHold1;
	struct time timeHold2;
	struct time result;
	bool enterError;
	result.hour = 0;
	result.minute = 0;
	result.second = 0;
	timeHold1.hour=atoi(argv[1]);
	timeHold2.hour=atoi(argv[2]);
	timeHold1.minute=atoi(argv[3]);
	timeHold2.minute=atoi(argv[4]);
	timeHold1.second=atoi(argv[5]);
	timeHold2.second=atoi(argv[6]);
	if (error(timeHold1, timeHold2) == true)
	{
		result = timeSub(timeHold1, timeHold2, result);
		cout << result.hour << " часов " << result.minute << " минут " << result.second << " секунд" << endl;
		system("pause");
	}
	else
	{
		cout << "Время введено не верно"<<endl;
		system("pause");
	}

    return 0;
}







